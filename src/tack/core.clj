;;   Copyright (c) Dimas Guardado, Jr. All rights reserved.

(ns tack.core
  (:require [clojure.string :as s]
            [imparsonate.core :as parser]
            [lamina.core :as ch]
            [gloss.core :as gloss]
            [aleph.tcp :as tcp])
  (:gen-class))

;; The database and transactions are represented usinging Clojure's Persistent data structures.
;; Maps for the DB, and lists for the transaction history.
;;
;; From the requirements:
;;
;; -----
;;
;; The most commonly used commands are GET, SET, UNSET and NUMEQUALTO, and each of these commands 
;; should be faster than O(N) expected worst case, where N is the number of total variables stored 
;; in the database.
;;
;; Typically, we will already have committed a lot of data when we begin a new transaction, but the 
;; transaction will only modify a few values. So, your solution should be efficient about how much 
;; memory is allocated for new transactions, i.e., it is bad if beginning a transaction nearly doubles 
;; your program's memory usage.
;;
;; ----
;;
;; I picked clojure to get all the simplicity of full copies of the db while still having efficient
;; retrieval, creation, and memory usage. For instance, lookup on a db key occurs in time comparable
;; to a HashMap. So we get efficient lookup by default. Since the map representing the db is immutable, 
;; the "copy" created to maintain transaction history is just a reference to the original value itself,
;; using no additional memory. Modifications to an in-transaction view of the database require little 
;; more than the space needed to hold the new key-value pairs.

;; Network connectivity is purely for fun and there's a ton of correctness issues for concurrent
;; transactions. Transaction commits are clobbering last-write-wins commits. The views of each TX will
;; not be merged, and the last transation to commit will represent the new value of the database.
;;
;; Console mode is correct according to the above specification because it assumes a single thread of execution.
;; (ie concurrent transactions are impossible)

;; DB and transaction objects/utils

;; Creates a Var that can be dynamically rebound per thread
(def ^:dynamic *transaction* nil)

;; Creates a mutable ref. connections is a map from a channel/connection to its transaction scopes
(def connections (ref {}))

;; The database is a mutable reference to an immutable map
(def db (ref {}))

(defn db-now [] (merge @db (peek @*transaction*)))

;; read operations

(defn GET [name]
  ;; looks up name as a key in the current view of the database
  ((db-now) name))

(defn NUMEQUALTO [value]
  ;; gets all the values from the current view of the database,
  ;; keeps only those values in the set containing only 'value' (#{...} is an immutable hash set literal)
  ;; and counts the resulting value sequence
  (count (filter #{value} (vals (db-now)))))

;; write operations

(defn SET [name value]
  (dosync
    (if (not (empty? @*transaction*))
      ;; update the current transaction view with the latest name-value association
      (alter *transaction* #(conj (rest %) (assoc (first %) name value)))
      ;; otherwise, update the current view of the database
      (alter db assoc name value))
    :ignore))

(defn UNSET [name]
  (dosync
    (if (not (empty? @*transaction*))
      ;; remove the name-value association from the current view of the transaction
      (alter *transaction* #(conj (rest %) (dissoc (first %) name)))
      ;; otherwise remove it from the current view of the database
      (alter db dissoc name))
    :ignore))

;; transaction management

(defn BEGIN []
  (dosync
    ;; create a new transaction scope. If the scope stack is empty, push a snapshot of the database to the stack
    ;; otherwise push a copy of the top of the stack onto itself
    (alter *transaction* #(conj % (or (peek %) @db)))
    :ignore))

(defn COMMIT []
  (dosync
    ;; replace the database with the view at the inner-most scope of the transaction stack and reset the stack
    (alter db (fn [db] (or (peek @*transaction*) db)))
    (ref-set *transaction* '())
    :ignore))

(defn ROLLBACK []
  (dosync
    ;; clear inner-most transaction scope by popping it off the stack
    (let [old @*transaction*]
      (alter *transaction* (fn [tx] (if (empty? tx) tx (pop tx))))
      (if (empty? old) :invalid :ignore))))

;; application

(defn END [] :exit)

;; parser + repl - uses a declarative grammar to parse db commands.
;; BTW, this is genuinely unnecessary, totally overkill, and pretty fun. :)

;; For whatever reason, this library doesn't seem to like whitespace separators.
(parser/defparser rd
  :root       :statement
  :statement  [:command :args?] #(cons %1 (if %2 (s/split %2 #"\s+") '()))
  ;; The regex is slightly more compact than set notation, but could have gone that way as
  ;; well. The function transformation looks up the var named by the parsed command in the
  ;; given namespace.
  :command    #"SET|GET|UNSET|NUMEQUALTO|BEGIN|COMMIT|ROLLBACK|END" #(ns-resolve 'tack.core (symbol %))
  :args       #".+")

(defn evl [statement]
  (if statement
    ;; a statement is a list with a function at its head the remaining items as arguments
    (let [[command & args] statement]
      (apply command args)) :ignore))

(defn response [result]
  ;; like switch
	(case result
   :exit    "\nSee you next time!\n"
   :invalid "INVALID ROLLBACK"
   :ignore  nil
   nil      "NULL"
   result))

;; console utils

(defn prnt [result]
  (let [resp (response result)]
    (and resp (println resp))
    result))


(defn mini-repl []
  ;; Reads a statement from stdin, evaluates it, and prints its result. Will loop until the result is :exit
  (let [result (-> (read-line) rd evl prnt)]
    (if (not= result :exit)
      (recur))))

(defn console []
  ;; In Console mode, we create a transaction ref that is bound to the main console thread
  (binding [*transaction* (ref '())]
    (println "Welcome to Tack DB! What is your command?\n")
    (mini-repl)))

;; server utils

(defn add-connection [ch]
  (dosync
    (alter connections assoc ch (ref []))))

(defn kill-connection [ch]
  (dosync
    (alter connections dissoc ch)))

(defn remote-exec [ch command]
  ;; Thread-local binding of *transaction* to the appropriate transaction object for this connection
  (binding [*transaction* (@connections ch)]
    (let [result (-> command rd evl)
          resp (response result)]
      ;; if there is a response, send it to the client
      (and resp (ch/enqueue ch resp))
      ;; closing a channel will terminate the connection
      (if (= result :exit) (ch/close ch)))))

(defn handler [ch client-info]
  (add-connection ch)
  (ch/on-drained ch #(kill-connection ch))
  (ch/receive-all ch (partial remote-exec ch)))

(defn server []
  (println "Starting Tack DB Server on port 10000")
  (tcp/start-tcp-server handler {:port 10000, :frame (gloss/string :utf-8 :delimiters ["\r\n"])}))

;; CLI

(defn -main
  "Starts Tack DB in console or server mode"
  [& args]
  (cond
    (or (empty? args) (= (first args) "console")) (console)
    (= (first args) "server") (server)
    :default (println "Invalid Mode: " (first args))))
