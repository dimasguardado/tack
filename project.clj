(defproject tack "0.1.0-SNAPSHOT"
  :description "A simple, transactional, in-memory key-value database"
  :url "http://github.com/dguardado/tack"
  :dependencies [[org.clojure/clojure "1.4.0"]
                 [imparsonate "0.2.0"]
                 [aleph "0.3.0-beta1"]]
  :main tack.core)
